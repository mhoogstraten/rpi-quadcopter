# -*- coding: utf-8 -*-
from time import sleep,time
from MPU6050.MPU6050 import MPU6050


#---------------------------------------------------------------------------
# write data
#---------------------------------------------------------------------------
updateoffset=True
IMU=MPU6050( 0x68 )
if updateoffset:
	IMU.updateOffsets('IMU_offset.txt')
IMU.readOffsets('IMU_offset.txt')
n=1
try:
    with open('sensor_data.csv', 'w+') as data_file:
		output=IMU.readSensors( 0 )
		
		data_file.write( '\t\t' )
		flag=False
		for k in output.keys():
			if flag:
				data_file.write( '\t' )
			data_file.write( k )
			flag = True
		flag=False
		data_file.write(';\n')
		
		inittime=time()
		tottime=0
		print 'IMU running...'
		for i in range (5000):
			tottime_old=tottime
			tottime=time()-inittime
			steptime=tottime-tottime_old

			starttime=time()
			output=IMU.readSensors( steptime )
			tottime=starttime-inittime

			res=str(n)+'\t'+str(tottime)
			for k in output.keys():
				res=res+'\t'+str(output[k])
			res=res+';\n'

			data_file.write(res)
			endtime=time()
			sleep(0.001)
			n=n+1
		data_file.flush()

except IOError, err:
	#logger.critical('Could not open offset config file: %s for writing', file_name)
	data_rc = False